class Customer < ActiveRecord::Base
  has_many :quotes
  accepts_nested_attributes_for :quotes

  def self.search(query)
    where("cust_lname like ? or cust_fname like ?", "%#{query}%", "%#{query}%")
  end

  def custfullname
    "#{cust_fname} #{cust_lname}"
  end
end
