class Quote < ActiveRecord::Base
  belongs_to :customer
  belongs_to :vehicle
  belongs_to :salesperson
  validates_inclusion_of :is_sold, :in => [true, false]


  def self.pay_years
    [3, 4, 5]
  end
end
