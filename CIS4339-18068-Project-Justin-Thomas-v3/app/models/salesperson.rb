class Salesperson < ActiveRecord::Base
  has_many :quotes
  accepts_nested_attributes_for :quotes

  def spfullname
    "#{sp_fname} #{sp_lname}"
  end
end
