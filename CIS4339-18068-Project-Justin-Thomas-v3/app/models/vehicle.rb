class Vehicle < ActiveRecord::Base
  has_many :quotes
  accepts_nested_attributes_for :quotes

  def self.search(query)
    where("model like ? or color like ? or vin like ?", "%#{query}%", "%#{query}%", "%#{query}%")
  end
end
