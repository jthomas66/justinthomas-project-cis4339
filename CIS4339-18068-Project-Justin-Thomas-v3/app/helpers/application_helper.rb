module ApplicationHelper
  def yesno(a)
    a ? "Yes" : "No"
  end
end
