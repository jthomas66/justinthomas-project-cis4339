json.array!(@vehicles) do |vehicle|
  json.extract! vehicle, :id, :model, :color, :vin, :wholesale_price
  json.url vehicle_url(vehicle, format: :json)
end
