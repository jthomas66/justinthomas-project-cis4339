json.array!(@salespeople) do |salesperson|
  json.extract! salesperson, :id, :sp_fname, :sp_lname
  json.url salesperson_url(salesperson, format: :json)
end
