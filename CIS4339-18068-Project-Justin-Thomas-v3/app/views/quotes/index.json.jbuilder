json.array!(@quotes) do |quote|
  json.extract! quote, :id, :customer_id, :vehicle_id, :salesperson_id, :sale_price, :interest_rate, :payment_years, :is_sold
  json.url quote_url(quote, format: :json)
end
