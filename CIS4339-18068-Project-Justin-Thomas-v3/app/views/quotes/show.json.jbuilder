json.extract! @quote, :id, :customer_id, :vehicle_id, :salesperson_id, :sale_price, :interest_rate, :payment_years, :is_sold, :created_at, :updated_at
