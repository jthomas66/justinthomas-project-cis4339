json.array!(@customers) do |customer|
  json.extract! customer, :id, :cust_fname, :cust_lname
  json.url customer_url(customer, format: :json)
end
