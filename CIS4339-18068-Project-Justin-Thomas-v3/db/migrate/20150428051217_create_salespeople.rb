class CreateSalespeople < ActiveRecord::Migration
  def change
    create_table :salespeople do |t|
      t.string :sp_fname
      t.string :sp_lname

      t.timestamps null: false
    end
  end
end
