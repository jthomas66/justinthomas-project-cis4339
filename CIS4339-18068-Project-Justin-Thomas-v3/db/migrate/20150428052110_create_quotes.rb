class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
      t.integer :customer_id
      t.integer :vehicle_id
      t.integer :salesperson_id
      t.decimal :sale_price
      t.decimal :interest_rate
      t.integer :payment_years
      t.boolean :is_sold

      t.timestamps null: false
    end
  end
end
