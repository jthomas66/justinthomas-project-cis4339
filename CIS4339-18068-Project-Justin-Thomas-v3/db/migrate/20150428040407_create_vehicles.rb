class CreateVehicles < ActiveRecord::Migration
  def change
    create_table :vehicles do |t|
      t.string :model
      t.string :color
      t.integer :vin
      t.decimal :wholesale_price

      t.timestamps null: false
    end
  end
end
