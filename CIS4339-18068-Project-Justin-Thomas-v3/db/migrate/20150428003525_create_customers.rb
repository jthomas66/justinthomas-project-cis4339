class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :cust_fname
      t.string :cust_lname

      t.timestamps null: false
    end
  end
end
