# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Vehicle.create(model: 'Charger', color: 'White', vin: 81831000, wholesale_price: 19885.00)
Vehicle.create(model: 'Charger', color: 'Blue', vin: 77885544, wholesale_price: 24000.00)
Vehicle.create(model: 'Charger SRT', color: 'Green', vin: 81831003, wholesale_price: 45599.00)
Vehicle.create(model: 'Charger R/T', color: 'White', vin: 45625810, wholesale_price: 25000.00)
Vehicle.create(model: 'Ram 1500', color: 'Black', vin: 12125454, wholesale_price: 30000.00)
Vehicle.create(model: 'Ram 2500', color: 'Silver', vin: 12312312, wholesale_price: 38000.00)
Vehicle.create(model: 'Dart', color: 'Red', vin: 78945612, wholesale_price: 16000.00)
Vehicle.create(model: 'Jeep', color: 'White', vin: 44445555, wholesale_price: 28000.00)
Vehicle.create(model: 'Durango', color: 'Navy', vin: 77552266, wholesale_price: 32500.00)
Vehicle.create(model: 'Neon', color: 'Purple', vin: 11111112, wholesale_price: 15999.00)